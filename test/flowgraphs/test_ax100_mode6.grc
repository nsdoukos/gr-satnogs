options:
  parameters:
    author: surligas
    catch_exceptions: 'True'
    category: '[GRC Hier Blocks]'
    cmake_opt: ''
    comment: ''
    copyright: ''
    description: ''
    gen_cmake: 'On'
    gen_linking: dynamic
    generate_options: no_gui
    hier_block_src_path: '.:'
    id: test_ax100_mode6
    max_nouts: '0'
    output_language: python
    placement: (0,0)
    qt_qss_theme: ''
    realtime_scheduling: ''
    run: 'True'
    run_command: '{python} -u {filename}'
    run_options: run
    sizing_mode: fixed
    thread_safe_setters: ''
    title: Test AX100 Mode 6
    window_size: (1000,1000)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 8]
    rotation: 0
    state: enabled

blocks:
- name: samp_rate
  id: variable
  parameters:
    comment: ''
    value: '32000'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [184, 12]
    rotation: 0
    state: enabled
- name: variable_ax100_mode6_decoder_0
  id: variable_ax100_mode6_decoder
  parameters:
    comment: ''
    crc_type: CRC32_C
    descramble: 'True'
    whitening: satnogs.whitening.make_ccsds(True)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [56, 316.0]
    rotation: 0
    state: true
- name: variable_ax100_mode6_encoder_0
  id: variable_ax100_mode6_encoder
  parameters:
    comment: ''
    crc_type: CRC32_C
    nrzi: 'True'
    postamble_len: '16'
    preamble_len: '16'
    scrambling: 'True'
    whitening: satnogs.whitening.make_ccsds(True)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [56, 172.0]
    rotation: 0
    state: true
- name: blocks_message_strobe_1
  id: blocks_message_strobe
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    msg: pmt.cons(pmt.PMT_NIL,pmt.intern("TEST"))
    period: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [792, 236.0]
    rotation: 180
    state: true
- name: blocks_packed_to_unpacked_xx_0
  id: blocks_packed_to_unpacked_xx
  parameters:
    affinity: ''
    alias: ''
    bits_per_chunk: '1'
    comment: ''
    endianness: gr.GR_MSB_FIRST
    maxoutbuf: '0'
    minoutbuf: '0'
    num_ports: '1'
    type: byte
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [832, 116.0]
    rotation: 0
    state: enabled
- name: blocks_tagged_stream_multiply_length_0
  id: blocks_tagged_stream_multiply_length
  parameters:
    affinity: ''
    alias: ''
    c: '8'
    comment: ''
    lengthtagname: packet_len
    maxoutbuf: '0'
    minoutbuf: '0'
    type: byte
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1040, 132.0]
    rotation: 0
    state: enabled
- name: digital_binary_slicer_fb_0
  id: digital_binary_slicer_fb
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1536, 356.0]
    rotation: 180
    state: enabled
- name: digital_burst_shaper_xx_0
  id: digital_burst_shaper_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    insert_phasing: 'False'
    length_tag_name: '"packet_len"'
    maxoutbuf: '0'
    minoutbuf: '0'
    post_padding: '100'
    pre_padding: '0'
    type: float
    window: ([])
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1504, 124.0]
    rotation: 0
    state: enabled
- name: digital_chunks_to_symbols_xx_0
  id: digital_chunks_to_symbols_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    dimension: '1'
    in_type: byte
    maxoutbuf: '0'
    minoutbuf: '0'
    num_ports: '1'
    out_type: float
    symbol_table: '[-1, 1]'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1296, 148.0]
    rotation: 0
    state: enabled
- name: pdu_pdu_to_tagged_stream_0
  id: pdu_pdu_to_tagged_stream
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    tag: packet_len
    type: byte
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [576, 124.0]
    rotation: 0
    state: enabled
- name: satnogs_ber_calculator_0
  id: satnogs_ber_calculator
  parameters:
    affinity: ''
    alias: ''
    comment: "For now FER should be 1, because the decoder keeps also the \nAX.25\
      \ header. This should be fixed in future iterations."
    frame_size: '64'
    maxoutbuf: '0'
    minoutbuf: '0'
    nframes: '100'
    skip: '10'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [568, 348.0]
    rotation: 180
    state: true
- name: satnogs_frame_decoder_0_0
  id: satnogs_frame_decoder
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    decoder_object: variable_ax100_mode6_decoder_0
    itype: byte
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1312, 360.0]
    rotation: 180
    state: enabled
- name: satnogs_frame_encoder_0
  id: satnogs_frame_encoder
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    encoder_object: variable_ax100_mode6_encoder_0
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [344, 124.0]
    rotation: 0
    state: true

connections:
- [blocks_message_strobe_1, strobe, satnogs_ber_calculator_0, trigger]
- [blocks_packed_to_unpacked_xx_0, '0', blocks_tagged_stream_multiply_length_0, '0']
- [blocks_tagged_stream_multiply_length_0, '0', digital_chunks_to_symbols_xx_0, '0']
- [digital_binary_slicer_fb_0, '0', satnogs_frame_decoder_0_0, '0']
- [digital_burst_shaper_xx_0, '0', digital_binary_slicer_fb_0, '0']
- [digital_chunks_to_symbols_xx_0, '0', digital_burst_shaper_xx_0, '0']
- [pdu_pdu_to_tagged_stream_0, '0', blocks_packed_to_unpacked_xx_0, '0']
- [satnogs_ber_calculator_0, pdu, satnogs_frame_encoder_0, pdu]
- [satnogs_frame_decoder_0_0, out, satnogs_ber_calculator_0, received]
- [satnogs_frame_encoder_0, pdu, pdu_pdu_to_tagged_stream_0, pdus]

metadata:
  file_format: 1
  grc_version: 3.10.8.0
