/*
 * Copyright 2023 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "pydoc_macros.h"
#define D(...) DOC(gr, satnogs, __VA_ARGS__)
/*
  This file contains placeholders for docstrings for the Python bindings.
  Do not edit! These were automatically extracted during the binding process
  and will be overwritten during the build process
 */


static const char* __doc_gr_satnogs_metadata = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_metadata_0 = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_metadata_1 = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_value = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_keys = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_time_iso8601 = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_add_time_iso8601 = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_add_pdu = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_add_crc_valid = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_add_sample_start = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_add_sample_cnt = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_add_symbol_erasures = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_add_corrected_bits = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_add_center_freq = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_add_freq_offset = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_add_snr = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_add_decoder_0 = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_add_decoder_1 = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_add_antenna_azimuth = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_add_antenna_elevation = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_add_antenna_polarization = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_add_phase_delay = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_add_resampling_ratio = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_add_symbol_timing_error = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_to_json = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_transform = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_to_file = R"doc()doc";


static const char* __doc_gr_satnogs_metadata_to_sting = R"doc()doc";
