/*
 * Copyright 2023 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "pydoc_macros.h"
#define D(...) DOC(gr, satnogs, __VA_ARGS__)
/*
  This file contains placeholders for docstrings for the Python bindings.
  Do not edit! These were automatically extracted during the binding process
  and will be overwritten during the build process
 */


static const char* __doc_gr_satnogs_decoder_status = R"doc()doc";


static const char* __doc_gr_satnogs_decoder_status_decoder_status_0 = R"doc()doc";


static const char* __doc_gr_satnogs_decoder_status_decoder_status_1 = R"doc()doc";


static const char* __doc_gr_satnogs_decoder = R"doc()doc";


static const char* __doc_gr_satnogs_decoder_decoder_0 = R"doc()doc";


static const char* __doc_gr_satnogs_decoder_decoder_1 = R"doc()doc";


static const char* __doc_gr_satnogs_decoder_unique_id = R"doc()doc";


static const char* __doc_gr_satnogs_decoder_decode = R"doc()doc";


static const char* __doc_gr_satnogs_decoder_reset = R"doc()doc";


static const char* __doc_gr_satnogs_decoder_input_multiple = R"doc()doc";


static const char* __doc_gr_satnogs_decoder_max_frame_len = R"doc()doc";


static const char* __doc_gr_satnogs_decoder_sizeof_input_item = R"doc()doc";


static const char* __doc_gr_satnogs_decoder_name = R"doc()doc";


static const char* __doc_gr_satnogs_decoder_version = R"doc()doc";
