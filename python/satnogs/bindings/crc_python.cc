/*
 * Copyright 2023 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

/***********************************************************************************/
/* This file is automatically generated using bindtool and can be manually edited  */
/* The following lines can be configured to regenerate this file during cmake      */
/* If manual edits are made, the following tags should be modified accordingly.    */
/* BINDTOOL_GEN_AUTOMATIC(0)                                                       */
/* BINDTOOL_USE_PYGCCXML(0)                                                        */
/* BINDTOOL_HEADER_FILE(crc.h)                                                     */
/* BINDTOOL_HEADER_FILE_HASH(0)                                                    */
/***********************************************************************************/

#include <pybind11/complex.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;

#include <gnuradio/satnogs/crc.h>
// pydoc.h is automatically generated in the build directory
#include <crc_pydoc.h>

void bind_crc(py::module& m)
{

    using crc = ::gr::satnogs::crc;

    using crc8 = CRC::Parameters<crcpp_uint8, 8>;
    using crc16 = CRC::Parameters<crcpp_uint16, 16>;
    using crc32 = CRC::Parameters<crcpp_uint32, 32>;

    py::class_<crc8>(m, "crc8_t");
    py::class_<crc16>(m, "crc16_t");
    py::class_<crc32>(m, "crc32_t");

    py::class_<crc> pycrc(m, "crc", D(crc));

    py::enum_<crc::type>(pycrc, "type")
        .value("NONE", crc::type::NONE)
        .value("CRC16_AUG_CCITT", crc::type::CRC16_AUG_CCITT)
        .value("CRC16_AUG_CCITT_XOR", crc::type::CRC16_AUG_CCITT_XOR)
        .value("CRC16_CMS", crc::type::CRC16_CMS)
        .value("CRC16_HDLC", crc::type::CRC16_HDLC)
        .value("CRC16_KERMIT", crc::type::CRC16_KERMIT)
        .value("CRC16_XMODEM", crc::type::CRC16_XMODEM)
        .value("CRC32_C", crc::type::CRC32_C)
        .export_values();

    pycrc
        .def_static(
            "append",
            (size_t(*)(crc::type, uint8_t*, const uint8_t*, size_t, bool))(&crc::append),
            py::arg("t"),
            py::arg("out"),
            py::arg("data"),
            py::arg("len"),
            py::arg("msb") = true,
            D(crc, append))
        .def_static("append",
                    (size_t(*)(const crc8&, uint8_t*, const uint8_t*, size_t, bool))(
                        &crc::append),
                    py::arg("t"),
                    py::arg("out"),
                    py::arg("data"),
                    py::arg("len"),
                    py::arg("msb") = true,
                    D(crc, append))
        .def_static("append",
                    (size_t(*)(const crc16&, uint8_t*, const uint8_t*, size_t, bool))(
                        &crc::append),
                    py::arg("t"),
                    py::arg("out"),
                    py::arg("data"),
                    py::arg("len"),
                    py::arg("msb") = true,
                    D(crc, append))
        .def_static("append",
                    (size_t(*)(const crc32&, uint8_t*, const uint8_t*, size_t, bool))(
                        &crc::append),
                    py::arg("t"),
                    py::arg("out"),
                    py::arg("data"),
                    py::arg("len"),
                    py::arg("msb") = true,
                    D(crc, append))
        .def_static("check",
                    (bool (*)(crc::type, const uint8_t*, size_t, bool))(&crc::check),
                    py::arg("t"),
                    py::arg("data"),
                    py::arg("len"),
                    py::arg("msb") = true,
                    D(crc, check))
        .def_static("check",
                    (bool (*)(const crc8&, const uint8_t*, size_t, bool))(&crc::check),
                    py::arg("t"),
                    py::arg("data"),
                    py::arg("len"),
                    py::arg("msb") = true,
                    D(crc, check))
        .def_static("check",
                    (bool (*)(const crc16&, const uint8_t*, size_t, bool))(&crc::check),
                    py::arg("t"),
                    py::arg("data"),
                    py::arg("len"),
                    py::arg("msb") = true,
                    D(crc, check))
        .def_static("check",
                    (bool (*)(const crc32&, const uint8_t*, size_t, bool))(&crc::check),
                    py::arg("t"),
                    py::arg("data"),
                    py::arg("len"),
                    py::arg("msb") = true,
                    D(crc, check))
        .def_static("crc8",
                    [](crcpp_uint8 polynomial,
                       crcpp_uint8 init,
                       crcpp_uint8 final_XOR,
                       bool reflect_input,
                       bool reflect_output) -> crc8 {
                        crc8 params = {
                            polynomial, init, final_XOR, reflect_input, reflect_output
                        };
                        return params;
                    })
        .def_static("crc16",
                    [](crcpp_uint16 polynomial,
                       crcpp_uint16 init,
                       crcpp_uint16 final_XOR,
                       bool reflect_input,
                       bool reflect_output) -> crc16 {
                        crc16 params = {
                            polynomial, init, final_XOR, reflect_input, reflect_output
                        };
                        return params;
                    })
        .def_static("crc32",
                    [](crcpp_uint32 polynomial,
                       crcpp_uint32 init,
                       crcpp_uint32 final_XOR,
                       bool reflect_input,
                       bool reflect_output) -> crc32 {
                        crc32 params = {
                            polynomial, init, final_XOR, reflect_input, reflect_output
                        };
                        return params;
                    });
}
