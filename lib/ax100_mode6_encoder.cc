/* -*- c++ -*- */
/*
 *  gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2021-2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/satnogs/ax100_mode6_encoder.h>
#include <gnuradio/satnogs/libfec/fec.h>

namespace gr {
namespace satnogs {

/**
 * @brief Creates a AX.100 Mode 5 encoder
 *
 * @param preamble_len the number of repetitions of the AX.25 SYNC flag at the start of
 * the frame
 * @param postamble_len the number of repetitions of the AX.25 SYNC flag at the end of the
 * frame
 * @param scrambler the scrambler that will scramble the payload <b>before</b> the AX.25
 * frame
 * @param ax25_scrambling if true, AX.25 will apply its own scrambling
 * @param nrzi if true output will be NRZI encoded
 * @param crc the CRC to be appended at the end of the payload. This CRC does not affect
 * in any way the AX.25 CRC
 * @return sptr shared pointer to the encoder object
 */
ax100_mode6_encoder::sptr ax100_mode6_encoder::make(size_t preamble_len,
                                                    size_t postamble_len,
                                                    whitening::sptr scrambler,
                                                    bool ax25_scrambling,
                                                    bool nrzi,
                                                    crc::type crc)
{
    return ax100_mode6_encoder::sptr(new ax100_mode6_encoder(
        preamble_len, postamble_len, scrambler, ax25_scrambling, nrzi, crc));
}

ax100_mode6_encoder::ax100_mode6_encoder(size_t preamble_len,
                                         size_t postamble_len,
                                         whitening::sptr scrambler,
                                         bool ax25_scrambling,
                                         bool nrzi,
                                         crc::type crc)
    : d_max_frame_len(255),
      d_ax25(
          "GND", 0x60, "GND", 0x61, preamble_len, postamble_len, ax25_scrambling, nrzi),
      d_scrambler(scrambler),
      d_crc(crc),
      d_pdu(new uint8_t[d_max_frame_len + crc::size(d_crc) + 32])
{
    if (!d_scrambler) {
        throw std::invalid_argument("Invalid scrambling object");
    }

    switch (d_crc) {
    case crc::type::NONE:
    case crc::type::CRC32_C:
        break;
    default:
        throw std::invalid_argument("ax100_mode6_encoder: Invalid CRC");
    }
}

ax100_mode6_encoder::~ax100_mode6_encoder() { delete[] d_pdu; }

pmt::pmt_t ax100_mode6_encoder::encode(pmt::pmt_t msg)
{
    pmt::pmt_t b;
    if (pmt::is_blob(msg)) {
        b = msg;
    } else if (pmt::is_pair(msg)) {
        b = pmt::cdr(msg);
    } else {
        throw std::runtime_error("ax100_mode6_encoder: received a malformed pdu message");
    }

    size_t pdu_len(0);
    const uint8_t* pdu = (const uint8_t*)pmt::uniform_vector_elements(b, pdu_len);
    if (pdu_len > d_max_frame_len) {
        throw std::runtime_error("ax100_mode6_encoder: PDU received has a size larger "
                                 "than the maximum allowed");
    }

    std::copy(pdu, pdu + pdu_len, d_pdu);
    pdu_len += crc::append(d_crc, d_pdu + pdu_len, d_pdu, pdu_len);

    // RS encoding
    encode_rs_8(d_pdu, d_pdu + pdu_len, 223 - pdu_len);
    pdu_len += 32;

    d_scrambler->reset();
    d_scrambler->scramble(d_pdu, d_pdu, pdu_len);

    pmt::pmt_t vecpmt(pmt::make_blob(d_pdu, pdu_len));
    pmt::pmt_t res(pmt::cons(pmt::PMT_NIL, vecpmt));

    // Return the AX.25 encoding msg
    return d_ax25.encode(res);
}

} /* namespace satnogs */
} /* namespace gr */
