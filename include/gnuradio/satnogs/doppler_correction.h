/* -*- c++ -*- */
/*
 * gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef DOPPLER_CORRECTION_H
#define DOPPLER_CORRECTION_H

#include <gnuradio/satnogs/api.h>
#include <memory>
#include <string>

namespace gr {
namespace satnogs {

/**
 * @brief Base class of the Doppler Correction
 *
 * This class provides the API for a generalized Doppler correction. Different correction
 * schemes can be implemented and easily used.
 */
class SATNOGS_API doppler_correction
{
public:
    using sptr = std::shared_ptr<doppler_correction>;

    doppler_correction(const std::string& name) : m_name(name) {}

    const std::string& name() const { return m_name; }

    virtual void start() = 0;

    virtual void stop() = 0;

    virtual double offset() = 0;

    virtual void reset() = 0;

private:
    const std::string m_name;
};

} // namespace satnogs
} // namespace gr


#endif // DOPPLER_CORRECTION_H
