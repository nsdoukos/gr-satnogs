/* -*- c++ -*- */
/*
 * gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2022 Libre Space Foundation <http://libre.space/>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gnuradio/satnogs/config.h>

namespace gr {
namespace satnogs {

using fec_input_buffer_type =
    std::conditional<config::gnuradio_version_num(config::gnuradio_major,
                                                  config::gnuradio_minor,
                                                  config::gnuradio_patch,
                                                  config::gnuradio_tweak) >=
                         config::gnuradio_version_num(3, 10, 11, 0),
                     const void*,
                     void*>::type;
}
} // namespace gr
