/* -*- c++ -*- */
/*
 *  gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2016-2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */


#ifndef INCLUDE_SATNOGS_AX25_H_
#define INCLUDE_SATNOGS_AX25_H_

#include <gnuradio/satnogs/crc.h>
#include <CRC.h>
#include <cstddef>
#include <cstdint>

namespace gr {

namespace satnogs {

class ax25
{
public:
    static constexpr size_t min_addr_len = 14;
    static constexpr size_t max_addr_len = (2 * 7 + 8 * 7);
    static constexpr size_t max_ctrl_len = 2;
    static constexpr size_t pid_len = 1;
    static constexpr size_t max_header_len = (max_addr_len + max_ctrl_len + pid_len);
    static constexpr uint8_t sync_flag = 0x7e;
    static constexpr size_t callsign_max_len = 6;

    /**
     * AX.25 Frame types
     */
    enum class frame_type {
        I_FRAME, //!< Information frame
        S_FRAME, //!< Supervisory frame
        U_FRAME, //!< Unnumbered frame
        UI_FRAME //!< Unnumbered information frame
    };

    static uint16_t crc(const uint8_t* buffer, size_t len)
    {
        const auto& crc = CRC::CRC_16_X25();
        return CRC::Calculate(buffer, len, crc);
    }

    static bool crc_valid(const uint8_t* buffer, size_t len)
    {
        const auto& crc = CRC::CRC_16_X25();
        auto fcs = CRC::Calculate(buffer, len - sizeof(uint16_t), crc);
        uint16_t recv_fcs = (((uint16_t)buffer[len - 1]) << 8) | buffer[len - 2];
        return recv_fcs == fcs;
    }
};

} // namespace satnogs
} // namespace gr

#endif /* INCLUDE_SATNOGS_AX25_H_ */
